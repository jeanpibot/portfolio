const next = require('next');

const express = require('express');
const  { config } = require('./config/index');
const dev =  config.dev;

const app = next({ dev })
const handle = app.getRequestHandler();

app.prepare()
    .then(() => {
        const server = express();

        server.get('*', (req, res) => {
            return handle(req, res);
        })
        server.listen(config.port, (err) => {
            if (err) throw err
                console.log(`> Ready on http://localhost:${config.port}`);
        })
    })
    .catch((ex) => {
        console.error(ex.stack)
        process.exit(1)
    })



