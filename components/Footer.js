import React from "react";

const Footer = () => {
    return (
        <div className="footer">
            <h1 className="md:text-3xl lg:text-4xl">Hire Me</h1>
            <span className="sm:text-base px-2 md:text-xl">I am Currently taking on new projects. Would you like to discuss yours ?</span>

            <div className="grid grid-flow-rows my-4">
                <h2 className="md:text-xl">Contact me</h2>
                <span className="text-gray-700">+57 3185274636</span>
                <span className="text-gray-700">+57 3024510763</span>
                <a className="text-gray-700" target="_blank" href="mailto:arenaspierre@protonmail.com">arenaspierre@protonmail.com</a>
            </div>
        </div>
    );
};

export default Footer;
