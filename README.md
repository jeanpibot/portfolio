#  Portfolio of work

It just is a simple Portfolio of work which was created  Next.js and React.js.  

![Captura del sitio web 0](./.readmeStatic/photo0.png)

![Captura del sitio web 1](./.readmeStatic/photo1.png)

![Captura del sitio web 2](./.readmeStatic/photo2.png)

![Captura del sitio web 3](./.readmeStatic/photo3.png)

![Captura del sitio web 4](./.readmeStatic/photo4.png)

## ¿How Does It Work?

* `yarn install` for install of dependencies
* `yarn dev` for development environment
* `yarn build && yarn start` for production environment

## Licence

MIT