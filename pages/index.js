import React from "react";
import Head from 'next/head';
import Intro from "../components/Intro";
import Header from '../components/Header';
import Services from "../components/Services";
import AboutMe from "../components/AboutMe";
import Work from "../components/Work";


class App extends React.Component {
    render() {
        return (
        <main>
            <Head>
                <title>Portfolio</title>
                <link rel="shortcut icon" href="../static/favicon.ico" />
            </Head>
            <Header></Header>
            <Intro></Intro>
            <Services></Services>
            <AboutMe></AboutMe>
            <Work></Work>
        </main>
        );
    }
}

export default App;
